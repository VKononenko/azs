<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?/*if($_POST['import_add'] or $_POST['import_edit']):?>
    <?ob_get_clean();
    echo json_encode([$arResult['HL_DATA']])?>
<?else:*/?>

<script type="text/javascript" src="/local/components/brr/soglas.m/templates/.default/script_soglas_m.js"></script>

<div id="center">
    <div class="filter-block red">
        <form action="" method="get">
            <div class="box dates">
                <div class="name">Дата</div>
                <div class="ins">
                    <div class="one lbox">
                        <input type="text" name="DATE_CHECK_FROM" value="<?=$arResult["FILTER"]["DATE_CHECK_FROM"]?>">
                        <a href="javascript:void()" class="cal_not_time" ></a>
                    </div>
                    <div class="sep"></div>
                    <div class="one lbox">
                        <input type="text" name="DATE_CHECK_TO" value="<?=$arResult["FILTER"]["DATE_CHECK_TO"]?>">
                        <a href="javascript:void()" class="cal_not_time"></a>
                    </div>
                </div>
            </div>
            <?if($arResult["GROUP"] != 10):?>
                <div class="box dates">
                    <div class="name">АЗС</div>
                    <div class="ins">
                        <div class="one lbox">
                            <input type="text" name="AZS_ID" value="<?=$arResult["FILTER"]["AZS_ID"]?>">
                        </div>
                    </div>
                </div>
                <div class="box dates">
                    <div class="name">Статус</div>
                    <div class="ins">
                        <div class="one lbox">
                            <select name="STATUS" style="height: 40px">
                                <option value=""></option>
                                <?foreach ($arResult['ENTER']['UF_STATUS'] as $i => $val):?>
                                    <?if ($i == $arResult["FILTER"]["STATUS"]):?>
                                        <option value = "<?=$i?>" selected><?=$val?></option>
                                    <?else:?>
                                        <option value = "<?=$i?>"><?=$val?></option>
                                    <?endif?>
                                <?endforeach?>
                            </select>
                        </div>
                    </div>
                </div>
                <?if($arResult["GROUP"] != 10 and $arResult["GROUP"] != 12):?>
                    <div class="box dates">
                        <div class="name">Территория №</div>
                        <div class="ins">
                            <div class="one lbox">
                                <input type="text" name="TERR_NUM" value="<?=$arResult["FILTER"]["TERR_NUM"]?>">
                            </div>
                        </div>
                    </div>
                <?endif?>
            <?endif?>
            <div class="box ok"><input type="submit" name="set_filter_1" value="Применить"></div>
            <input type="hidden" name="set_filter" value="Y"/>
        </form>
    </div>

    <?if ($arResult["GROUP"] == 10):?>
        <br><br><h1 style="text-align: center">Интерфейс менеджера АЗС № <?=$arResult['ENTER']['USER_NUM_FROM_LOGIN']?></h1><br><br>
    <?else:?>
        <?if ($arResult["GROUP"] == 13):?>
            <br><br><h1 style="text-align: center"><a href="/soglas/orr">Интерфейс ОРР</a></h1>
        <?elseif ($arResult["GROUP"] == 12):?>
            <br><br><h1 style="text-align: center"><a href="/soglas/terr">Интерфейс территориального менеджера</a></h1>
        <?elseif ($arResult["GROUP"] == 11):?>
            <br><br><h1 style="text-align: center"><a href="/soglas/reg">Интерфейс регионального менеджера</a></h1>
        <?elseif ($arResult["GROUP"] == 1):?>
            <br><br><h1 style="text-align: center"><a href="/soglas">Интерфейс администратора</a></h1>
        <?endif?>
        <h2 style="text-align: center">Раздел АЗС<?=(!empty($arResult["FILTER"]["AZS_ID"]) ? ' № '.$arResult["FILTER"]["AZS_ID"] : '')?></h2><br><br>
    <?endif?>

    <?if (($arResult["GROUP"] == 11 or $arResult["GROUP"] == 12) and $arResult['ENABLE_BUTTON'] == 1):?>
        <h4 style="text-align: center">Внимание! При отправке, либо при удалении всех зеленых строк - возможность редактирования завершается!</h4>
    <?endif?>


    <form method="POST" id="form">
        <?if ($arResult['ENABLE_BUTTON'] == 1):?>

            <?
            $func = 'add()';
            if ($arResult["GROUP"] != 10 and empty($arResult['FILTER']['AZS_ID'])){
                $func = 'alrt_add()';
            }
            ?>

            <div style="text-align: center">
                <input style="width: 90%; background: azure;" type="button" value="+ ДОБАВИТЬ СТРОКУ +" title = 'Добавить новую строку' onClick='<?=$func?>'>
                <br><div style="font-size: 16px">Каждая строка должна содержать уникальное сочетание вида оплаты + вида товара, иначе данные не будут записаны</div>
            </div>
        <?endif?>

        <table class="rating-table" id='tabl'>

            <tr>
                <td>ID</td>
                <?foreach($arResult['HL_HEAD_FORM'] as $key => $value):?>
                    <td> <?=$value?> </td>
                <?endforeach?>
                <?if ($arResult['HAVE_RETURN']):?>
                    <td>Исправить ДО</td>
                <?endif?>
            </tr>


            <?foreach($arResult['HL_DATA'] as $key => $value):?>
                <?if ($arResult['ENABLE_BUTTON'] == 1 and  // Вывод редактируемых строк
                    (($arResult["GROUP"] == 10 and $value['UF_STATUS'] == 1 and $value['UF_STATUS_TM'] != 2 and $value['UF_STATUS_TM'] != 3)
                    or ($arResult["GROUP"] == 12 and ($value['UF_STATUS'] == 3 or $value['UF_STATUS'] == 1))
                    or ($arResult["GROUP"] == 11 and ($value['UF_STATUS'] == 3 or $value['UF_STATUS_TM'] == 3 or $value['EDIT_FOR_REG'] == true) and $value['UF_STATUS_REG'] != 2))):?>
                    <tr class="edit_manager_row">
                        <?foreach($value as $key2 => $value2):?>
                            <?//echo '<pre>';var_dump($value);echo '</pre>';?>
                            <?if ($key2 != 'UF_STATUS' and $key2 != 'UF_STATUS_TM' and $key2 != 'UF_STATUS_REG' and $key2 != 'EDIT_FOR_REG'): // Не показывать?>
                                <?if ($key2 == 'ID'):?>
                                    <td>
                                        <input size="<?=strlen($value2)?>" name="id_edit[]" value = "<?=$value2?>" readonly class="input_clean">
                                    </td>
                                <?elseif ($key2 == 'UF_AZS'):?>
                                    <td>
                                        <input size="<?=strlen($value2)?>" name="azs_edit[]" value = "<?=$value2?>" readonly class="input_clean">
                                    </td>
                                <?elseif ($key2 == 'UF_VID_OPL'):?>
                                    <td>
                                        <select class="select_enter" required name="sel_vid_opl_edit[]">
                                            <option value=""></option>
                                            <?foreach ($arResult['ENTER']['OPLATA'] as $i => $val):?>
                                                <?if ($val == $value2):?>
                                                    <option value = "<?=$i?>" selected><?=$val?></option>
                                                <?else:?>
                                                    <option value = "<?=$i?>"><?=$val?></option>
                                                <?endif?>
                                            <?endforeach?>
                                        </select>
                                    </td>
                                <?elseif ($key2 == 'UF_VID_TOVARA'):?>
                                    <td>
                                        <select class="select_enter" required name="sel_vid_tov_edit[]">
                                            <option value=""></option>
                                            <?foreach ($arResult['ENTER']['GOODS'] as $i => $val):?>
                                                <?if ($val == $value2):?>
                                                    <option value = "<?=$i?>" selected><?=$val?></option>
                                                <?else:?>
                                                    <option value = "<?=$i?>"><?=$val?></option>
                                                <?endif?>
                                            <?endforeach?>
                                        </select>
                                    </td>
                                <?elseif ($key2 == 'UF_PLAN'):?>
                                    <td>
                                        <input size="<?=strlen($value2)?>" class="select_enter" required type="number" name="plan_edit[]" autocomplete="off" step="0.001" min="0" value="<?=$value2?>">
                                    </td>
                                <?elseif ($key2 == 'UF_COMMENT' ):?>
                                    <td style="max-width: 400px">
                                        <?if(empty($value2)):?>•<?else:?><textarea class="textarea_clean" name="comment_edit[]" <?//rows="1" без php?>  readonly><?=$value2?></textarea><?endif?>
                                    </td>
                                <?elseif ($key2 == 'TIMER_THREE' and $arResult['HAVE_RETURN'] == 1):?>
                                    <td style="background: #e2383f; color: white;">
                                        <?if (empty($value2)):?>
                                            •
                                        <?else:?>
                                            <input size="<?=strlen($value2)?>" name="timer[]" value = "<?=$value2?>" readonly class="input_clean" style="color: white">
                                        <?endif?>
                                    </td>
                                <?elseif ($key2 == 'TIMER_THREE'):?>
                                <?else:?>
                                    <td><?=$value2?></td>
                                <?endif?>
                            <?endif?>
                        <?endforeach?>
                        <td class="del_col" style="background: #ff444c">
                            <input type="submit" value="Х" title="Удалить строку № <?=$value['ID']?> навсегда" name="del<?=$value['ID']?>"  style="padding: 0; margin: 0; color: white; background: transparent; border: 0; width: auto;">
                        </td>
                    </tr>
                <?else: //Строки без редактирования ?>
                    <tr>
                        <?foreach($value as $key2 => $value2):?>
                            <?if ($key2 != 'UF_STATUS' and $key2 != 'UF_STATUS_TM' and $key2 != 'UF_STATUS_REG' and $key2 != 'EDIT_FOR_REG'):?> <!-- Не показывать статус -->
                                <?if ($key2 == 'ID'):?> <!-- Ячейка ID -->
                                    <td><output value = "<?=$value2?>" name="id"><?=$value2?></output></td>
                                <?elseif ($key2 == 'UF_COMMENT'):?> <!-- Ячейка ID -->
                                    <td style="max-width: 250px;"><?=(empty($value2)?'•': $value2)?></td>
                                <?elseif ($key2 == 'TIMER_THREE' and $arResult['HAVE_RETURN'] == 1):?>
                                    <td><?=empty($value2) ? '•' : $value2?></td>
                                <?elseif ($key2 == 'TIMER_THREE'):?>
                                <?else:?>
                                    <td><?=$value2?></td>
                                <?endif?>
                            <?endif?>
                        <?endforeach?>
                    </tr>
                <?endif?>
            <?endforeach?>
        </table>

        <?if ($arResult['HAVE_RETURN'] == 1):?>
            <script>autoMergeByRow('tabl',1,8,9);</script>
        <?else:?>
            <script>autoMergeByRow('tabl',1,8,8);</script>
        <?endif?>

        <br>

        <?if ($arResult["GROUP"] == 10 and $arResult['ENABLE_BUTTON'] == 1):?>
        <div style="text-align: center">
            <input type="submit" style="background: palegreen;" value="Сохранить" id="save_button" name="save_button" title = 'Записать данные с возможностью дальнейшего редактирования до наступления 6го числа'>
            <input type="submit" style="background: #ff444c; color: white" value="Отправить" name="send_button" id="send_button" title = 'Отправить данные. Возможность редактирования закрывается'>
        </div>
        <br>
        <table style="margin: 0 auto; text-align: center">
            <tr><td style="background: azure; border: 0">Строка, выделенная этим цветом, добавляет данные</td></tr>
            <tr><td style="background: palegreen; border: 0">Строка, выделенная этим цветом, изменяет данные</td></tr>
            <tr><td style="border: 0">Для отображения комментария целиком - растяните ячейку</td></tr>
        </table>
        <?elseif($arResult['ENABLE_BUTTON'] == 1):?>
            <div style="text-align: center">
                <input type="submit" style="background: #ff444c; color: white" value="Отправить" name="send_button" id="send_button" title = 'Отправить данные. Возможность редактирования закрывается'>
            </div>
        <?endif?>
    </form>
</div>

<?// ДОБАВЛЕННАЯ СТРОКА ?>
<script type="template" id="text">
    <tr class="add_manager_row">
        <td><output name="id">ID</output></td>
        <td><?=$arResult['ENTER']['CUR_DATE']?></td>
        <td><?=$arResult['ENTER']['MONTHS'][$arResult['ENTER']['CUR_MONTH_NUM']]?></td>
        <td>
            <?if ($arResult["GROUP"] == 10):?>
                <?=$arResult['ENTER']['KSSS']?>
            <?else:?>
                auto
            <?endif?>
        </td>
        <td>
            <?if ($arResult["GROUP"] == 10):?>
                <?=$arResult['ENTER']['USER_NUM_FROM_LOGIN']?>
            <?else:?>
                <input size="<?=strlen($arResult['FILTER']['AZS_ID'])?>" name="list_azs" value = "<?=$arResult['FILTER']['AZS_ID']?>" readonly class="input_clean">
            <?endif?>

        </td>
        <td>
            <select class="select_enter" required name="sel_vid_opl[]">
                <option value=""></option>
                <?foreach ($arResult['ENTER']['OPLATA'] as $i => $val):?>
                    <option value = "<?=$i?>"><?=$val?></option>
                <?endforeach?>
            </select>
        </td>
        <td>
            <select class="select_enter" required name="sel_vid_tov[]">
                <option value=""></option>
                <?foreach ($arResult['ENTER']['GOODS'] as $i => $val):?>
                    <option value = "<?=$i?>"><?=$val?></option>
                <?endforeach?>
            </select>
        </td>
        <td>
            <input class="select_enter" required type="number" name="plan[]" autocomplete="off" step="0.001" min="0">
        </td>
        <td>•</td>
        <?=($arResult['HAVE_RETURN'] != true)?:('<td>•</td>')?>
        <td class="del_col" >
            <input type="button" value="Х" id="remove_row" title="Убрать строку" style="padding: 0; margin: 0; background: transparent; border: 0; width: auto;">
        </td>
    </tr>
</script>
