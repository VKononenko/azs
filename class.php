<?php
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class ExampleCompSimple extends CBitrixComponent {
    private $_request;

    private function _checkModules() {
        if (   !Loader::includeModule('iblock')
            || !Loader::includeModule('brr.tp')
        ) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }

        return true;
    }
    public function executeComponent() {
        $this->_checkModules();
        $this->_request = Application::getInstance()->getContext()->getRequest();

        global $USER;
        $arFilter = array();
        $CurUserID = CUser::GetID();
        $CurUserMail = CUser::GetEmail();
        $CurUserLogin = $USER->GetLogin();
        $CurUserFIO = CUser::GetFullName();

        require_once('functions.php');

        // Получение группы
        $this->arResult["GROUP"] = f_get_group();


        if (isset($_GET["set_filter"])) {

            //Дата
            if (isset($_GET["DATE_CHECK_FROM"]) && CheckDateTime($_GET["DATE_CHECK_FROM"]) &&
                empty($_GET["DATE_CHECK_TO"])) {

                $arFilter[">=UF_DATA"] = date('d.m.Y 00:00:00', MakeTimeStamp($_GET["DATE_CHECK_FROM"], "DD.MM.YYYY HH:MI:SS"));
                $this->arResult["FILTER"]["DATE_CHECK_FROM"] = $_GET["DATE_CHECK_FROM"];
            }

            if (isset($_GET["DATE_CHECK_TO"]) && CheckDateTime($_GET["DATE_CHECK_TO"]) &&
                empty($_GET["DATE_CHECK_FROM"])) {

                $arFilter["<=UF_DATA"] = date('d.m.Y 23:59:59', MakeTimeStamp($_GET["DATE_CHECK_TO"], "DD.MM.YYYY HH:MI:SS"));
                $this->arResult["FILTER"]["DATE_CHECK_TO"] = $_GET["DATE_CHECK_TO"];
            }

            if (isset($_GET["DATE_CHECK_FROM"]) && CheckDateTime($_GET["DATE_CHECK_FROM"]) &&
                isset($_GET["DATE_CHECK_TO"]) && CheckDateTime($_GET["DATE_CHECK_TO"]) &&
                MakeTimeStamp($_GET["DATE_CHECK_FROM"], "DD.MM.YYYY") <= MakeTimeStamp($_GET["DATE_CHECK_TO"], "DD.MM.YYYY HH:MI:SS")
            ) {
                $arFilter[">=UF_DATA"] = date('d.m.Y 00:00:00', MakeTimeStamp($_GET["DATE_CHECK_FROM"], "DD.MM.YYYY HH:MI:SS"));
                $arFilter["<=UF_DATA"] = date('d.m.Y 23:59:59', MakeTimeStamp($_GET["DATE_CHECK_TO"], "DD.MM.YYYY HH:MI:SS"));

                $this->arResult["FILTER"]["DATE_CHECK_FROM"] = $_GET["DATE_CHECK_FROM"];
                $this->arResult["FILTER"]["DATE_CHECK_TO"] = $_GET["DATE_CHECK_TO"];
            }

            // Админ, Орр, Рег, Тер
            if ($this->arResult["GROUP"] != 10){
                if (intval($_GET['AZS_ID']) > 0) {
                    $arFilter['UF_AZS'] = intval($_GET['AZS_ID']);
                    $this->arResult["FILTER"]["AZS_ID"] = $_GET['AZS_ID'];
                }

                if (!empty($_GET['STATUS'])) {
                    $arFilter['UF_STATUS'] = $_GET['STATUS'];
                    $this->arResult["FILTER"]["STATUS"] = $_GET['STATUS'];
                }

                if(!empty($_GET['TERR_NUM'])){
                    $arFilter['UF_TERR_NUM'] = $_GET['TERR_NUM'];
                    $this->arResult["FILTER"]["TERR_NUM"] = $_GET['TERR_NUM'];
                }
            }
        }
        else{
            $arFilter[">=UF_DATA"] = date('01.m.Y');
            $arFilter["<=UF_DATA"] = date('t.m.Y 23:59:59');

            $this->arResult["FILTER"]["DATE_CHECK_FROM"] = date('01.m.Y');
            $this->arResult["FILTER"]["DATE_CHECK_TO"] = date('t.m.Y');
        }


        //Текущее время
        $objDateTime = new \Bitrix\Main\Type\DateTime;
        $this->arResult['ENTER']['CUR_DATE'] = $objDateTime->format("d.m.Y");
        $this->arResult['ENTER']['CUR_DAY'] = f_cur_day();


        if (in_array($this->arResult["GROUP"], [10,11,12])) {

            // Номер АЗС или Терр через логин (для АЗС и ТМ)
            if ($this->arResult["GROUP"] == 10 or $this->arResult["GROUP"] == 12){
                $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'] = f_login_num($CurUserLogin);
            }


            if ($this->arResult["GROUP"] == 10) {

                // KSSS и TERRITORY ID - ТОЛЬКО ДЛЯ М!
                $arKsssTerId = f_get_ksss($this->arResult['ENTER']['USER_NUM_FROM_LOGIN']);
                $this->arResult['ENTER']['KSSS'] = $arKsssTerId['KSSS'];
                $this->arResult['ENTER']['TERRITORY_ID'] = $arKsssTerId['TERRITORY_ID'];
            }


            if ($this->arResult["GROUP"] == 10 or $this->arResult["GROUP"] == 12){

                // ФИО, Почта ТМ (+ № терр для М)
                $arTMInfo = f_get_tm_info($this->arResult["GROUP"], $this->arResult['ENTER']['TERRITORY_ID'], $CurUserID);

                $this->arResult['ENTER']['TERR_NAME'] = $arTMInfo['TERR_NAME'];
                $this->arResult['ENTER']['TERR_MAIL'] = $arTMInfo['TERR_MAIL'];

                if ($this->arResult["GROUP"] == 10){
                    $this->arResult['ENTER']['TERRITORY_NUM'] = $arTMInfo['TERRITORY_NUM'];
                }
                else if ($this->arResult["GROUP"] == 12){
                    $this->arResult['ENTER']['TERRITORY_ID'] = $arTMInfo['TERRITORY_ID'];
                }
            }



            // ID региона для РЕГ
            if ($this->arResult["GROUP"] == 11){
                $this->arResult['REGION_ID'] = f_get_reg_id($CurUserID);
            }

            // Регион ID для М и ТМ, а также ID терр для РЕГ
            Bitrix\Main\Loader::includeModule('Brr.Tp');
            $rsReg = \Brr\Tp\territory\territoryTable::getList(array());
            while($arReg = $rsReg->fetch()){
                if($arReg['ID'] == $this->arResult['ENTER']['TERRITORY_ID']){ // ID территории совпало
                    $this->arResult['REGION_ID'] = $arReg['REGION_ID']; // Получаем ID региона
                }
            }


            // РЕГ Почта и ID
            Bitrix\Main\Loader::includeModule('Brr.Tp');
            $rsReg = \Brr\Tp\region\regionTable::getList(array());
            while($arReg = $rsReg->fetch()){
                if($this->arResult['REGION_ID'] == $arReg['ID']){ // ID польз совпали
                    $this->arResult['REGION_USER_ID'] = $arReg['USER_ID'];
                    $arUserRegData = CUser::GetByID($this->arResult['REGION_USER_ID'])->Fetch();
                    $this->arResult['ENTER']['REG_MAIL'] = $arUserRegData['EMAIL'];
                    break;
                }
            }

            // Получение номера территории
            if ($this->arResult["GROUP"] == 12){
                $this->arResult['ENTER']['TERRITORY_NUM'] = $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'];
            }
        }

        //Фильтры
        if ($this->arResult["GROUP"] == 10){ // М
            //$arFilter["UF_CREATED_ID"] = $CurUserID;
            $arFilter["UF_AZS"] = $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'];
        }
        else if ($this->arResult["GROUP"] == 12){ // ТМ
            $arFilter["UF_TERR_NUM"] = $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'];
        }
        else if ($this->arResult["GROUP"] == 11){ // РЕГ
            $arFilter["UF_REGION_ID"] = $this->arResult['REGION_ID'];
        }

        // Цифра текущего месяца
        $this->arResult['ENTER']['CUR_MONTH_NUM'] = date("n");

        // Месяцы
        $this->arResult['ENTER']['MONTHS'] = f_monthsList();

        // Список статусов
        $this->arResult['ENTER']['UF_STATUS'] = f_status_list();

        // Виды оплат и товаров
        $this->arResult['ENTER']['OPLATA'] = f_vid_opl_tov_list()['OPL'];
        $this->arResult['ENTER']['GOODS'] = f_vid_opl_tov_list()['TOV'];

        // Получение листа подконтрольных АЗС для ТМ и РЕГ
        if ($this->arResult["GROUP"] == 12 or $this->arResult["GROUP"] == 11){
            $this->arResult['UNDER_AZS'] = f_under_azs($this->arResult["GROUP"], $this->arResult['REGION_ID'], $this->arResult['ENTER']['USER_NUM_FROM_LOGIN']);
        }

        // Подконтрольные АЗС, выводимые после даты отправки, доступные для добавления в строке +
        if ($this->arResult["GROUP"] == 12 /*and $this->arResult['ENTER']['CUR_DAY'] >= 6*/){
            $this->arResult['UNDER_AZS_ON'] = f_get_azs_status($this->arResult['UNDER_AZS'], $this->arResult["GROUP"]);
        }

        // Для рега просрочки М, либо с возвратом от РЕГа
        else if ($this->arResult["GROUP"] == 11 /*and f_reg_time() == true*/){
            $this->arResult['UNDER_AZS_ON'] = f_get_azs_status($this->arResult['UNDER_AZS'], $this->arResult["GROUP"]);
        }

        //echo '<pre>';var_dump($this->arResult['UNDER_AZS_ON']);echo '</pre>';


        //Подготовка HighBlock:
        if (CModule::IncludeModule('highloadblock')) {
            $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
            $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            // Журнал
            $arHLBlockJ = Bitrix\Highloadblock\HighloadBlockTable::getById(7)->fetch();
            $obEntityJ = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlockJ);
            $strEntityDataClassJ = $obEntityJ->getDataClass();
        }


        if ($this->arResult["GROUP"] == 10 or $this->arResult["GROUP"] == 12){ // М или ТМ
            $arSelectManagerFields = array('UF_STATUS','UF_STATUS_TM','ID','UF_DATA','UF_TERR_NUM','UF_KSSS','UF_AZS','UF_VID_OPL','UF_VID_TOVARA','UF_PLAN', 'UF_COMMENT', 'UF_RETURN');
        }
        else{ // РЕГ, ОРР, АДМИН
            $arSelectManagerFields = array('UF_STATUS','UF_STATUS_TM','UF_STATUS_REG','ID','UF_DATA','UF_TERR_NUM','UF_KSSS','UF_AZS','UF_VID_OPL','UF_VID_TOVARA','UF_PLAN', 'UF_COMMENT', 'UF_REGION_ID', 'UF_RETURN');
        }

        // ШАПКА ФОРМЫ
        $entity = 'HLBLOCK_' . 1;
        $dbRes = CUserTypeEntity::GetList(array(),array(
                'ENTITY_ID' => $entity, //'FIELD_NAME' => 'UF_DATA',
                'LANG' => 'ru'
            )
        );

        while ($arRes = $dbRes->Fetch()) {
            if (in_array($arRes["ID"], [8, 9, 13, 32, 31, 28, 39])){  // ID нужных полей
                if (in_array($arRes["ID"], [8])){
                    $this->arResult['HL_HEAD_FORM'][] = 'Месяц';
                }
                $this->arResult['HL_HEAD_FORM'][] = $arRes["LIST_COLUMN_LABEL"];
            }
        }

        //$arUserData = CUser::GetByID($ksss_send['AZS_ID'])->Fetch();



        // ЗАГРУЗКА ДАННЫХ В ТАБЛИЦУ
        if (CModule::IncludeModule('highloadblock')) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                //echo '<pre>';var_dump($_POST);echo '</pre>';

                // Удаление строки
                foreach ($_POST as $key_check => $value_check){
                    if (strpos($key_check, 'del') !== false){
                        $str_id = preg_replace("/[^,.0-9]/", '', $key_check);
                        $strEntityDataClass::delete($str_id);
                        header('location: ' . $_SERVER['REQUEST_URI']);
                        break;
                    }
                }

                $zapret = 0;

                if (empty($str_id)){

                    // Совпадения
                    $arCopy = [];

                    foreach ($_POST["plan_edit"] as $ey => $al){
                        $str_check = $_POST["sel_vid_tov_edit"][$ey].$_POST["sel_vid_opl_edit"][$ey];

                        if (in_array($str_check, $arCopy)){
                            $zapret = 1;
                            break;
                        }
                        $arCopy[] = $str_check;

                    }
                    foreach ($_POST["plan"] as $ey2 => $al2){
                        $str_check = $_POST["sel_vid_tov"][$ey2].$_POST["sel_vid_opl"][$ey2];

                        if (in_array($str_check, $arCopy)){
                            $zapret = 1;
                            break;
                        }
                        $arCopy[] = $str_check;
                    }

                    // Если все ок
                    if ($zapret == 0) {

                        $arJournalAzs = [];

                        // Запись НОВЫХ строк
                        foreach ($_POST['sel_vid_opl'] as $key => $val) {
                            echo '1';
                            if ($this->arResult["GROUP"] == 10) { // Для М
                                $arElementFields = array(
                                    'UF_DATA' => $objDateTime->format("d.m.Y H:i:s"),
                                    'UF_KSSS' => $this->arResult['ENTER']['KSSS'],
                                    'UF_AZS' => $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],

                                    'UF_VID_OPL' => $_POST['sel_vid_opl'][count($_POST['sel_vid_opl']) - $key - 1], // Для загрузки в обратном порядке
                                    'UF_VID_TOVARA' => $_POST['sel_vid_tov'][count($_POST['sel_vid_opl']) - $key - 1],
                                    'UF_PLAN' => $_POST['plan'][count($_POST['sel_vid_opl']) - $key - 1],

                                    'UF_STATUS' => isset($_POST['send_button']) ? 2 : 1, // Статус М при отправке Готов (2), при сохранении - Проект (1)
                                    'UF_STATUS_TM' => 1,
                                    'UF_STATUS_REG' => 1,

                                    'UF_CREATED_ID' => $CurUserID,
                                    'UF_CREATED_BY' => $CurUserLogin,
                                    'UF_CREATED_MAIL' => $CurUserMail,
                                    'UF_TERR' => $this->arResult['ENTER']['TERRITORY_ID'],
                                    'UF_TERR_NUM' => $this->arResult['ENTER']['TERRITORY_NUM'],
                                    'UF_TERR_USER_NAME' => $this->arResult['ENTER']['TERR_NAME'],
                                    'UF_TM_MAIL' => $this->arResult['ENTER']['TERR_MAIL'],
                                    'UF_REGION_ID' => $this->arResult['REGION_ID'],
                                    'UF_REG_MAIL' => $this->arResult['ENTER']['REG_MAIL'],
                                    'UF_FIO_AZS' => $CurUserFIO,
                                );

                                $arJournalAzs[] = $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'];
                            } else { // Для РЕГ и ТМ

                                $ksss_send = f_get_ksss($_POST['list_azs']); //$_POST['list_azs'][count($_POST['sel_vid_opl']) - $key - 1]

                                // Данные ТМ для рег
                                if ($this->arResult["GROUP"] == 11) {

                                    $arTMInfo = f_get_tm_info($this->arResult["GROUP"], $ksss_send['TERRITORY_ID'], 0);

                                    $this->arResult['ENTER']['TERR_NAME'] = $arTMInfo['TERR_NAME'];
                                    $this->arResult['ENTER']['TERR_MAIL'] = $arTMInfo['TERR_MAIL'];
                                    $this->arResult['ENTER']['TERRITORY_NUM'] = $arTMInfo['TERRITORY_NUM'];

                                }

                                $azs_num = mb_strlen($ksss_send['AZS_ID']) >= 5 ? $ksss_send['AZS_ID'] : str_pad($ksss_send['AZS_ID'], 5, '0', STR_PAD_LEFT);
                                $arUserData = CUser::GetByLogin('azs' . $azs_num)->Fetch();

                                // Проверяем статус АЗС в журнале
                                $rsData3 = $strEntityDataClassJ::getList(array(
                                    'select' => array('UF_DATE', 'UF_AZS', 'UF_ANNOT', 'UF_NEW_STATUS', 'UF_NEW_STATUS_TM', 'UF_NEW_STATUS_REG', 'UF_COMMENT', 'UF_CREATE_GROUP'),
                                    'order' => array('UF_DATE' => 'DESC'),
                                    'filter' => array(
                                        '>=UF_DATE' => $objDateTime->format("01.m.Y"),
                                        '<=UF_DATE' => $objDateTime->format("t.m.Y 23:59:59"),
                                        'UF_AZS' => $_POST['list_azs'],
                                        '!%UF_ANNOT' => 'D:',
                                    ),
                                ));
                                // находим последнюю запись этой строки
                                while ($arItem_SEL_NEW = $rsData3->Fetch()) {
                                    break;
                                }


                                $arElementFields = array(
                                    'UF_DATA' => $objDateTime->format("d.m.Y H:i:s"),
                                    'UF_KSSS' => $ksss_send['KSSS'],
                                    'UF_AZS' => $_POST['list_azs'],
                                    'UF_VID_OPL' => $_POST['sel_vid_opl'][count($_POST['sel_vid_opl']) - $key - 1], // Для загрузки в обратном порядке
                                    'UF_VID_TOVARA' => $_POST['sel_vid_tov'][count($_POST['sel_vid_opl']) - $key - 1],
                                    'UF_PLAN' => $_POST['plan'][count($_POST['sel_vid_opl']) - $key - 1],
                                    'UF_STATUS' => 2,
                                    'UF_STATUS_TM' => 2,
                                    'UF_STATUS_REG' => ($arItem_SEL_NEW['UF_ANNOT'] == 'return_orr_button' ? 2 : 1),
                                    'UF_CREATED_ID' => $arUserData['ID'], // ID М
                                    'UF_CREATED_BY' => $arUserData['LOGIN'], // Получить Логин М
                                    'UF_CREATED_MAIL' => $arUserData['EMAIL'], // Получить Почту М
                                    'UF_FIO_AZS' => $arUserData['NAME'] . ' ' . $arUserData['LAST_NAME'] . ' ' . $arUserData['SECOND_NAME'],
                                    'UF_TERR' => $ksss_send['TERRITORY_ID'], //$this->arResult['ENTER']['TERRITORY_ID'],
                                    'UF_TERR_NUM' => $this->arResult['ENTER']['TERRITORY_NUM'],
                                    'UF_TERR_USER_NAME' => $this->arResult['ENTER']['TERR_NAME'],
                                    'UF_TM_MAIL' => $this->arResult['ENTER']['TERR_MAIL'],
                                    'UF_REGION_ID' => $this->arResult['REGION_ID'],
                                    'UF_REG_MAIL' => $this->arResult['ENTER']['REG_MAIL'],
                                );

                                $arJournalAzs[] = $_POST['list_azs'];
                            }
                            empty($arElementFields) ?: $strEntityDataClass::add($arElementFields);
                        }

                        if (!empty($arJournalAzs) and empty($_POST['id_edit'])) {
                            foreach (array_unique($arJournalAzs) as $azs) {
                                // Заносим в журнал
                                $arElementFieldsEditJ = array(
                                    'UF_DATE' => $objDateTime->format("d.m.Y H:i:s"),
                                    'UF_AZS' => $azs,
                                    'UF_CREATE_ID' => $CurUserID,
                                    'UF_CREATE_GROUP' => $this->arResult["GROUP"],
                                    'UF_CREATE_FIO' => $CurUserFIO,
                                    'UF_NUM' => $this->arResult["GROUP"] == 11 ? $this->arResult['REGION_ID'] : $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],
                                    'UF_NEW_STATUS' => $this->arResult["GROUP"] == 10 ? (isset($_POST['send_button']) ? 2 : 1) : 2,
                                    'UF_NEW_STATUS_TM' => $this->arResult["GROUP"] == 10 ? 1 : 2,
                                    'UF_NEW_STATUS_REG' => ($this->arResult["GROUP"] == 11 and $arItem_SEL_NEW['UF_ANNOT'] == 'return_orr_button') ? 2 : 1,
                                    'UF_ANNOT' => $this->arResult["GROUP"] == 10 ? (isset($_POST['send_button']) ? 'send button' : 'save button') : 'send button',
                                    'UF_COMMENT' => '',
                                    'UF_LOGS' => 'M_S_01',
                                );
                                $strEntityDataClassJ::add($arElementFieldsEditJ);
                            }
                        }


                        // При Update проверяем статус АЗС в журнале
                        $rsData3 = $strEntityDataClassJ::getList(array(
                            'select' => array('UF_DATE', 'UF_AZS', 'UF_ANNOT', 'UF_NEW_STATUS', 'UF_NEW_STATUS_TM', 'UF_NEW_STATUS_REG', 'UF_COMMENT', 'UF_CREATE_GROUP'),
                            'order' => array('UF_DATE' => 'DESC'),
                            'filter' => array(
                                '>=UF_DATE' => $objDateTime->format("01.m.Y"),
                                '<=UF_DATE' => $objDateTime->format("t.m.Y 23:59:59"),
                                'UF_AZS' => $_POST['azs_edit'][0],
                                '!%UF_ANNOT' => 'D:',
                            ),
                        ));
                        // находим последнюю запись этой строки
                        while ($arItem_SEL = $rsData3->Fetch()) {
                            break;
                        }

                        $arJournalSend = [];

                        // Если нашли
                        if (!empty($arItem_SEL)) {

                            // ДЛЯ М
                            if ($this->arResult["GROUP"] == 10) {

                                //echo '<pre>';var_dump($_POST);echo '</pre>';

                                foreach ($_POST['id_edit'] as $key => $val) {

                                    // Если статус Готов или Просрочен - запись в журнал
                                    if ($arItem_SEL['UF_NEW_STATUS'] == 2 or $arItem_SEL['UF_NEW_STATUS'] == 3) {
                                        echo '2';
                                        $arElementFieldsEdit = []; // Таблица
                                        $arJournalSend['M_NOT_EDIT'][] = $_POST['azs_edit'][$key]; // Журнал
                                    } // Возврат и время не истекло (день от 6го, статус М "Проект", запись от РЕГ или ТМ время подачи не истекло)
                                    else if (/*($this->arResult['ENTER']['CUR_DAY'] >= 6) and*/ $arItem_SEL['UF_NEW_STATUS'] == 1 and ($arItem_SEL['UF_CREATE_GROUP'] == 11 or $arItem_SEL['UF_CREATE_GROUP'] == 12) and date("d.m.Y H:i:s") <= date("d.m.Y H:i:s", AddToTimeStamp(array("HH" => 3), MakeTimeStamp($arItem_SEL['UF_DATE'], "DD.MM.YYYY HH:MI:SS")))) {
                                        echo '3';
                                        $arElementFieldsEdit = array(
                                            'UF_VID_OPL' => $_POST['sel_vid_opl_edit'][$key],
                                            'UF_VID_TOVARA' => $_POST['sel_vid_tov_edit'][$key],
                                            'UF_PLAN' => $_POST['plan_edit'][$key],
                                            'UF_STATUS' => isset($_POST['send_button']) ? 2 : 1, // При отправке Готов, при сохранении - Проект
                                            // Если нажали Отправить И кнопка РЕГа - оставляем возврат, иначе 0
                                            'UF_RETURN' => isset($_POST['send_button']) ? ($arItem_SEL['UF_ANNOT'] != 'return_ter_button' ? 1 : 0) : 1,
                                            // Если Отправить - проверяем от кого сделан возврат. Иначе не меняем
                                            'UF_COMMENT' => isset($_POST['send_button']) ? ($arItem_SEL['UF_ANNOT'] != 'return_ter_button' ? $arItem_SEL["UF_COMMENT"] : '') : $arItem_SEL["UF_COMMENT"],
                                        );
                                        // При отправке меняется статус, потому запись в журнал
                                        if (isset($_POST['send_button'])) {
                                            $arJournalSend['SEND_M_RETURN'][] = $_POST['azs_edit'][$key];
                                        }
                                        if ($arItem_SEL['UF_ANNOT'] == 'return_reg_button') {
                                            $arJournalSend['SEND_M_RETURN_TERR'][] = $_POST['azs_edit'][$key];
                                        }
                                    } // Если день 6 и выше, статус М "Проект", возврат от РЕГ или ТМ и время подачи истекло - Ставим Просрочку
                                    else if (/*($this->arResult['ENTER']['CUR_DAY'] >= 6) and*/ $arItem_SEL['UF_NEW_STATUS'] == 1 and ($arItem_SEL['UF_CREATE_GROUP'] == 11 or $arItem_SEL['UF_CREATE_GROUP'] == 12) and date("d.m.Y H:i:s") > date("d.m.Y H:i:s", AddToTimeStamp(array("HH" => 3), MakeTimeStamp($arItem_SEL['UF_DATE'], "DD.MM.YYYY HH:MI:SS")))) {
                                        echo '4';
                                        $arElementFieldsEdit = array(
                                            'UF_STATUS' => 3,
                                            'UF_COMMENT' => '',
                                            'UF_RETURN' => 0,
                                            'UF_PLAN' => 0,
                                        );
                                        $arJournalSend['SEND_M_RETURN_FAIL'][] = $_POST['azs_edit'][$key];
                                    } // Если просто Проект или никакого статуса или не нашли?
                                    else {
                                        echo '5';
                                        $arElementFieldsEdit = array(
                                            'UF_VID_OPL' => $_POST['sel_vid_opl_edit'][$key],
                                            'UF_VID_TOVARA' => $_POST['sel_vid_tov_edit'][$key],
                                            'UF_PLAN' => $_POST['plan_edit'][$key],
                                            'UF_STATUS' => isset($_POST['send_button']) ? 2 : 1, // Статус при отправке Готов (2), при сохранении - Проект (1)
                                            'UF_STATUS_TM' => 1,
                                            'UF_STATUS_REG' => 1,
                                            'UF_COMMENT' => isset($_POST['send_button']) ? "" : $_POST['comment_edit'][$key],
                                        );

                                        if (isset($_POST['send_button'])) {
                                            $arJournalSend['SEND_M_ELSE'][] = $_POST['azs_edit'][$key];
                                        }
                                    }

                                    //echo '<pre>';var_dump($arElementFieldsEdit);echo '</pre>';

                                    empty($arElementFieldsEdit) ?: $strEntityDataClass::update($val, $arElementFieldsEdit);
                                }

                                // Для найденных строк и М (не менять)
                                $arJournalSend['M_NOT_EDIT'] = array_unique($arJournalSend['M_NOT_EDIT'])[0];
                                $arJournalSend['SEND_M_RETURN'] = array_unique($arJournalSend['SEND_M_RETURN'])[0];
                                $arJournalSend['SEND_M_RETURN_FAIL'] = array_unique($arJournalSend['SEND_M_RETURN_FAIL'])[0];
                                $arJournalSend['SEND_M_OTHER'] = array_unique($arJournalSend['SEND_M_OTHER'])[0];
                                $arJournalSend['SEND_M_ELSE'] = array_unique($arJournalSend['SEND_M_ELSE'])[0];

                                // Отправка письма ТМу об отправке М возвращенных данных от РЕГ
                                $arMailSend['SEND_M_RETURN_TERR'] = array_unique($arJournalSend['SEND_M_RETURN_TERR'])[0];

                                if (!empty($arMailSend['SEND_M_RETURN_TERR'])){
                                    CEvent::Send("NEW_FORUM_PRIVATE_MESSAGE", 's1', array(
                                        "TO_EMAIL" => 'kononenko.v@asteq.ru', // вместо почты $this->arResult['ENTER']['TERR_MAIL']
                                        "TO_NAME" => $this->arResult['ENTER']['TERR_NAME'],
                                        "MESSAGE" => ' № '.$arMailSend['SEND_M_RETURN_TERR'],
                                    ), 'Y', 26);
                                }


                                foreach ($arJournalSend as $topic => $azs) {

                                    if (!empty($azs)) {
                                        $arElementFieldsEditJ = [];
                                        $arElementFieldsEditJ = array(
                                            'UF_DATE' => $objDateTime->format("d.m.Y H:i:s"),
                                            'UF_AZS' => $azs,
                                            'UF_CREATE_ID' => $CurUserID,
                                            'UF_CREATE_GROUP' => $this->arResult["GROUP"],
                                            'UF_CREATE_FIO' => $CurUserFIO,
                                            'UF_NUM' => $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],
                                            'UF_NEW_STATUS' => $arItem_SEL["UF_NEW_STATUS"],
                                            'UF_NEW_STATUS_TM' => $arItem_SEL["UF_NEW_STATUS_TM"],
                                            'UF_NEW_STATUS_REG' => $arItem_SEL["UF_NEW_STATUS_REG"],
                                            'UF_ANNOT' => '',
                                            'UF_COMMENT' => '',
                                            'UF_LOGS' => '',
                                        );

                                        if ($topic == 'M_NOT_EDIT') {
                                            $arElementFieldsEditJ['UF_ANNOT'] = isset($_POST['send_button']) ? "send_button D:" . $arItem_SEL['UF_NEW_STATUS'] : "save_button D:" . $arItem_SEL['UF_NEW_STATUS'];
                                            $arElementFieldsEditJ['UF_LOGS'] = 'M_R_02';
                                        } else if ($topic == 'SEND_M_RETURN') {
                                            $arElementFieldsEditJ['UF_NEW_STATUS'] = 2;
                                            $arElementFieldsEditJ['UF_ANNOT'] = 'send button';
                                            $arElementFieldsEditJ['UF_COMMENT'] = isset($_POST['send_button']) ? ($arItem_SEL['UF_ANNOT'] != 'return_ter_button' ? $arItem_SEL["UF_COMMENT"] : '') : $arItem_SEL["UF_COMMENT"];
                                            $arElementFieldsEditJ['UF_LOGS'] = 'M_R_03';

                                        } else if ($topic == 'SEND_M_RETURN_FAIL') {
                                            $arElementFieldsEditJ['UF_NEW_STATUS'] = 3;
                                            $arElementFieldsEditJ['UF_ANNOT'] = 'some button';
                                            $arElementFieldsEditJ['UF_LOGS'] = 'M_R_04';
                                        } else if ($topic == 'SEND_M_ELSE') {
                                            $arElementFieldsEditJ['UF_NEW_STATUS'] = 2;
                                            $arElementFieldsEditJ['UF_NEW_STATUS_TM'] = 1;
                                            $arElementFieldsEditJ['UF_NEW_STATUS_REG'] = 1;
                                            $arElementFieldsEditJ['UF_ANNOT'] = 'send_button';
                                            $arElementFieldsEditJ['UF_LOGS'] = 'M_R_05';
                                        }

                                        //echo '<pre>';var_dump($arElementFieldsEditJ);echo '</pre>';

                                        // Запись в журнал
                                        empty($arElementFieldsEditJ) ?: $strEntityDataClassJ::add($arElementFieldsEditJ);
                                    }
                                }
                            } // Для ТМ и РЕГ (строка найдена со статусом М Готов)
                            else if ($this->arResult["GROUP"] == 11 or $this->arResult["GROUP"] == 12) {

                                if ($arItem_SEL['UF_NEW_STATUS'] == 2 and $arItem_SEL['UF_ANNOT'] != 'return_orr_button') {
                                    echo '6';
                                    //echo $arItem_SEL['UF_ANNOT'];
                                    foreach ($_POST['id_edit'] as $key => $val) {
                                        $arJournalSend['TM_REG_NOT_EDIT'][] = $_POST['azs_edit'][$key]; // Журнал
                                    }

                                    $arJournalSend['TM_REG_NOT_EDIT'] = array_unique($arJournalSend['TM_REG_NOT_EDIT']);

                                    foreach ($arJournalSend['TM_REG_NOT_EDIT'] as $id => $azs) {
                                        $arElementFieldsEditJ = [];
                                        if (!empty($azs)) {
                                            $arElementFieldsEditJ = array(
                                                'UF_DATE' => $objDateTime->format("d.m.Y H:i:s"),
                                                'UF_AZS' => $azs,
                                                'UF_CREATE_ID' => $CurUserID,
                                                'UF_CREATE_GROUP' => $this->arResult["GROUP"],
                                                'UF_CREATE_FIO' => $CurUserFIO,
                                                'UF_NUM' => $this->arResult["GROUP"] == 11 ? $this->arResult['REGION_ID'] : $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],
                                                'UF_NEW_STATUS' => 2,
                                                'UF_NEW_STATUS_TM' => 2,
                                                'UF_NEW_STATUS_REG' => $this->arResult["GROUP"] == 12 ? 1 : 2,
                                                'UF_ANNOT' => "send_button D:" . $arItem_SEL['UF_NEW_STATUS'],
                                                'UF_COMMENT' => '',
                                                'UF_LOGS' => 'M_R_06',
                                            );
                                            empty($arElementFieldsEditJ) ?: $strEntityDataClassJ::add($arElementFieldsEditJ);
                                        }
                                    }
                                } else {
                                    echo '8';
                                    goto else_step;
                                }
                            }
                        } // АЗС не найдена в журнале (ДЛЯ ВСЕХ) или переход с 8
                        else {
                            else_step:
                            echo '9';
                            foreach ($_POST['id_edit'] as $key => $val) {
                                $arElementFieldsEdit = array(
                                    'UF_VID_OPL' => $_POST['sel_vid_opl_edit'][$key],
                                    'UF_VID_TOVARA' => $_POST['sel_vid_tov_edit'][$key],
                                    'UF_PLAN' => $_POST['plan_edit'][$key],
                                    'UF_STATUS' => isset($_POST['send_button']) ? 2 : 1, // Статус при отправке Готов (2), при сохранении - Проект (1)
                                    'UF_STATUS_TM' => $this->arResult["GROUP"] == 10 ? 1 : 2,
                                    'UF_STATUS_REG' => ($this->arResult["GROUP"] == 11 and $arItem_SEL['UF_ANNOT'] == 'return_orr_button') ? 2 : 1, //$this->arResult["GROUP"] == 10 ? 1 : ($this->arResult["GROUP"] == 12 ? 1 : 2),
                                    'UF_COMMENT' => isset($_POST['send_button']) ? "" : $_POST['comment_edit'][$key],
                                    'UF_RETURN' => 0,
                                );

                                if (isset($_POST['send_button'])) {
                                    $arJournalSend['SEND_ELSE_ALL'][] = $_POST['azs_edit'][$key];
                                }

                                empty($arElementFieldsEdit) ?: $strEntityDataClass::update($val, $arElementFieldsEdit);
                            }

                            $arJournalSend['SEND_ELSE_ALL'] = array_unique($arJournalSend['SEND_ELSE_ALL']);

                            foreach ($arJournalSend['SEND_ELSE_ALL'] as $id => $azs) {
                                $arElementFieldsEditJ = [];
                                if (!empty($azs)) {
                                    $arElementFieldsEditJ = array(
                                        'UF_DATE' => $objDateTime->format("d.m.Y H:i:s"),
                                        'UF_AZS' => $azs,
                                        'UF_CREATE_ID' => $CurUserID,
                                        'UF_CREATE_GROUP' => $this->arResult["GROUP"],
                                        'UF_CREATE_FIO' => $CurUserFIO,
                                        'UF_NUM' => $this->arResult["GROUP"] == 11 ? $this->arResult['REGION_ID'] : $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],
                                        'UF_NEW_STATUS' => isset($_POST['send_button']) ? 2 : 1,
                                        'UF_NEW_STATUS_TM' => $this->arResult["GROUP"] == 10 ? 1 : 2,
                                        'UF_NEW_STATUS_REG' => 1, // $this->arResult["GROUP"] == 10 ? 1 : ($this->arResult["GROUP"] == 12 ? 1 : 2),
                                        'UF_ANNOT' => isset($_POST['send_button']) ? 'send button' : 'save button',
                                        'UF_COMMENT' => isset($_POST['send_button']) ? "" : $_POST['comment_edit'][$key],
                                        'UF_LOGS' => 'M_R_07',
                                    );
                                    empty($arElementFieldsEditJ) ?: $strEntityDataClassJ::add($arElementFieldsEditJ);
                                }
                            }
                        }
                    }
                    else{
                        echo '<script>alert("Найдены совпадения сочетания вида товара + вида оплаты. Отредактируйте существующие строки, либо удалите их.");location.href=location.href;</script>';
                    }
                }
                if ($zapret != 1){
                    header('location: ' . $_SERVER['REQUEST_URI']);
                }
            }
            //echo '<pre>';var_dump($this->arResult['IMPORT']);echo '</pre>';



            // ВЫГРУЗКА ДАННЫХ - ФОРМА

            $rsData = $strEntityDataClass::getList(array(
                'select' => $arSelectManagerFields,
                'order' => array('ID' => 'DESC'),
                'filter' => $arFilter,
            ));

            // в некоторых полях замена на тектовый формат
            while ($arItem = $rsData->Fetch()) {
                //echo '<pre>';var_dump($arItem);echo '</pre>';

                // Формат даты
                $arItem["UF_DATA"] = $arItem["UF_DATA"]->format("d.m.Y");


                // Текстовый вид оплаты и товара
                $arVidText = f_get_vid_text($this->arResult['ENTER']['OPLATA'], $this->arResult['ENTER']['GOODS'], $arItem["UF_VID_OPL"], $arItem["UF_VID_TOVARA"]);
                $arItem["UF_VID_TOVARA"] = $arVidText['TOV'];
                $arItem["UF_VID_OPL"] = $arVidText['OPL'];


                // Вывод времени
                if ($arItem['UF_RETURN'] == 1){ // Если возврат

                    $this->arResult['HAVE_RETURN'] = true;

                    $rsData3 = $strEntityDataClassJ::getList(array(
                        'select' => array('UF_DATE','UF_AZS','UF_ANNOT'), // Дата записи, ID строки, Кнопка
                        'order' => array('UF_DATE' => 'DESC'),
                        'filter' => array(
                            '>=UF_DATE' => $objDateTime->format("01.m.Y"),
                            '<=UF_DATE' => $objDateTime->format("t.m.Y 23:59:59"),
                            'UF_AZS' => $arItem["UF_AZS"],
                            'UF_ANNOT' => (array('return_ter_button', 'return_reg_button', 'return_orr_button')),
                        ),
                    ));
                    while ($arItem2 = $rsData3->Fetch()) { // находим последнюю запись с этой строкой и добавляем 3 часа
                        if ($arItem2['UF_ANNOT'] == 'return_orr_button'){
                            $this->arResult['AZS_FOR_REG_BY_ORR'][] = $arItem['UF_AZS'];
                            $this->arResult['EDIT_FOR_REG'] = true;
                            $arItem['EDIT_FOR_REG'] = true;
                            $arItem['TIMER_THREE'] = date("d.m.Y H:i:s", AddToTimeStamp(array("DD" => 2), MakeTimeStamp($arItem2['UF_DATE'], "DD.MM.YYYY HH:MI:SS")));
                        }
                        else{
                            $arItem['TIMER_THREE'] = date("H:i:s", AddToTimeStamp(array("HH" => 3), MakeTimeStamp($arItem2['UF_DATE'], "DD.MM.YYYY HH:MI:SS")));

                            if ($arItem2['UF_ANNOT'] == 'return_reg_button'){
                                $this->arResult['EDIT_BY_REG'] = true;
                                $this->arResult['AZS_FOR_TM_BY_REG'][] = $arItem['UF_AZS'];
                            }
                        }
                        break;
                    }
                }

                if ($this->arResult['EDIT_FOR_REG'] == true and $this->arResult['GROUP'] != 11){
                    $arItem['UF_COMMENT'] = '';
                }



                if ($arItem['UF_STATUS'] == 3){ // Если есть просрочки М
                    $this->arResult['HAVE_GAME_OVER_M'] = true;
                }

                if ($arItem['UF_STATUS_TM'] == 3){ // Если есть просрочки ТМ
                    $this->arResult['HAVE_GAME_OVER_TM'] = true;
                }

                if ($arItem['UF_STATUS_TM'] == 2){ // Если есть отправка ТМ
                    $this->arResult['TM_HAVE_STATUS_SEND'] = true;
                }
                if ($arItem['UF_STATUS_REG'] != 2){ // Если есть статусы РЕГ помимо Готов
                    $this->arResult['REG_HAVE_STATUS_OTH'] = true;
                }
                if ($arItem['UF_STATUS_TM'] != 2){ // Если есть статусы ТМ помимо Готов
                    $this->arResult['TM_HAVE_STATUS_OTH'] = true;
                }



                !empty($arItem['TIMER_THREE']) ?: ($arItem['TIMER_THREE'] = '');

                // Для рега
                if ($this->arResult["GROUP"] == 12 and $arItem["UF_STATUS"] == 1){ // Если статус Проект у М для РЕГа
                    $this->arResult['CUR_USER_HAVE_STATUS_PROJECT'] = 1;
                }

                // Или если статусов вообще нет
                //$this->arResult['CUR_USER_HAVE_STATUS'][] = $arItem["UF_STATUS"];

                // Убираем лишнее
                unset($arItem["UF_TERR_NUM"]);
                unset($arItem["UF_REGION_ID"]);
                unset($arItem["UF_RETURN"]);

                // Загоняем месяц
                $arMonthText = $this->arResult['ENTER']['MONTHS'][date_parse_from_format("d.m.Y", $arItem["UF_DATA"])['month']];

                $cut = 4;
                if ($this->arResult["GROUP"] == 11){
                    $cut = 5;
                }

                $arItem = array_slice($arItem, 0, $cut, true) +
                    array("UF_MONTH" => "$arMonthText") +
                    array_slice($arItem, $cut, count($arItem) - 1, true);


                $this->arResult['HL_DATA'][] = $arItem;
            }

            $this->arResult['AZS_FOR_REG_BY_ORR'] = array_unique($this->arResult['AZS_FOR_REG_BY_ORR']);


            if ($this->arResult['GROUP'] == 10){
                // Получение всех статусов записей пользователя за текущий месяц, для вывода кнопок для пустых записей
                $rsData = $strEntityDataClass::getList(array(
                    'select' => array('UF_DATA', 'UF_STATUS', 'UF_AZS'),
                    'order' => array('ID' => 'DESC'),
                    'filter' => array(
                        ">=UF_DATA" => date('01.m.Y'),
                        "<=UF_DATA" => date('t.m.Y 23:59:59'),
                        "UF_AZS" => $this->arResult['ENTER']['USER_NUM_FROM_LOGIN'],
                    ),
                ));
                while ($arItem = $rsData->Fetch()) {
                    if ($arItem["UF_STATUS"] == 1){
                        $this->arResult['CUR_USER_HAVE_STATUS_PROJECT'] = 1;
                    }
                    $this->arResult['CUR_USER_HAVE_STATUS'][] = $arItem["UF_STATUS"];
                }
            }


            /*
            Условия для вывода кнопок (описание не обновлено):
            Для М - есть хотя бы одна запись в этом месяце со статусом Проект, либо нет вообще записей с каким-либо статусом и текущий день
            Для ТМ - есть просроченные записи у М и дата до конца 6 числа
            Для РЕГ - есть просроченные записи у М или ТМ и дата до 7 числа 8-00 (добавить возврат от ОРР)
            */
            if (($this->arResult['GROUP'] == 10 and ($this->arResult['CUR_USER_HAVE_STATUS_PROJECT'] == 1 or empty($this->arResult['CUR_USER_HAVE_STATUS'])) and $this->arResult['HAVE_GAME_OVER_TM'] != true and $this->arResult['TM_HAVE_STATUS_SEND'] != true)
                or ($this->arResult['GROUP'] == 12 and (($this->arResult['HAVE_GAME_OVER_M'] and $this->arResult['ENTER']['CUR_DAY'] <= 6) or ($this->arResult['CUR_USER_HAVE_STATUS_PROJECT'] == 1 and $this->arResult['EDIT_BY_REG'] == true))
                                                    and $this->arResult['TM_HAVE_STATUS_OTH'] == true)
                or ($this->arResult['GROUP'] == 11 and (($this->arResult['HAVE_GAME_OVER_M'] or $this->arResult['HAVE_GAME_OVER_TM'] and f_reg_time() == false) or $this->arResult['EDIT_FOR_REG'] == true)
                                                    and $this->arResult['REG_HAVE_STATUS_OTH'] == true and $this->arResult['UNDER_AZS_ON'][0] != NULL)){
                    $this->arResult['ENABLE_BUTTON'] = 1;
            }
        }

        if ($this->arResult['GROUP'] == 11 and empty($this->arResult['HL_DATA']) and f_reg_time() == true and $this->arResult['UNDER_AZS_ON'][0] != NULL){
            $this->arResult['ENABLE_BUTTON'] = 1;
        }

        //echo '<pre>';var_dump($this->arResult['HL_DATA']);echo '</pre>';
        $this->includeComponentTemplate();
    }
}
